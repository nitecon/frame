package web

import (
	"net/http"

	"gitlab.com/nitecon/frame/rest"
	"gitlab.com/nitecon/frame/users"
)

type StatusData struct {
	ServerStatus string `json:"serverStatus"`
	ServerState  int    `json:"serverState"`
}

func (s *StatusData) Scan(sd *StatusData) {
	s.ServerStatus = sd.ServerStatus
	s.ServerState = sd.ServerState
}

// RestGetBase returns the base rest response at /.
func RestGetBase(w *rest.Data, r *http.Request, u *users.User) {
	//q := rest.NewQuery(r))
	// use query to parse params etc.
	s := &StatusData{ServerStatus: "online", ServerState: 200}
	// use local scan function to update response struct (StatusData)
	//s.Scan(u)
	w.RenderData(s)
}
