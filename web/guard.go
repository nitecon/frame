package web

import (
	"compress/gzip"
	"io"
	"net/http"
	"strings"

	"github.com/julienschmidt/httprouter"
	"github.com/rs/zerolog/log"
	"gitlab.com/nitecon/frame/rest"
	"gitlab.com/nitecon/frame/users"
)

// AuthenticatedRestHandler defines an interface for handling authenticated requests
type AuthenticatedRestHandler interface {
	Handle(w *rest.Data, r *http.Request, u *users.User)
}

// AuthenticatedRestHandlerFunc is an adapter that allows a function to be used as an AuthenticatedRestHandler
type AuthenticatedRestHandlerFunc func(w *rest.Data, r *http.Request, u *users.User)

func (f AuthenticatedRestHandlerFunc) Handle(w *rest.Data, r *http.Request, u *users.User) {
	f(w, r, u)
}

// Gzip Response Writer struct
type gzipResponseWriter struct {
	io.Writer
	http.ResponseWriter
}

// Override Write method to compress the response
func (w gzipResponseWriter) Write(b []byte) (int, error) {
	return w.Writer.Write(b)
}

// GuardRestHandler is similar to GuardHandler however it is used for rest calls and does not include the PageData injection.
func GuardRestHandler(fn func(w *rest.Data, r *http.Request, u *users.User), permission string) httprouter.Handle {
	handler := &AuthenticatedRestHandlerAdapter{
		Permission: permission,
		Handler:    AuthenticatedRestHandlerFunc(fn),
	}

	return func(hw http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		if !strings.Contains(r.Header.Get("Accept-Encoding"), "gzip") {
			handler.ServeHTTP(hw, r)
		}
		hw.Header().Set("Content-Encoding", "gzip")
		gz := gzip.NewWriter(hw)
		defer gz.Close()
		gzr := gzipResponseWriter{Writer: gz, ResponseWriter: hw}
		handler.ServeHTTP(gzr, r)
	}
}

// AuthenticatedRestHandlerAdapter is an adapter for bridging a rest.AuthenticatedRestHandler into a normal http.Handler
type AuthenticatedRestHandlerAdapter struct {
	Handler    AuthenticatedRestHandler
	Permission string
}

func (a *AuthenticatedRestHandlerAdapter) ServeHTTP(hw http.ResponseWriter, r *http.Request) {
	handler := a.Handler
	var out = rest.Data{Writer: hw}
	if a.Permission == rest.PermSystemRead {
		handler.Handle(&out, r, nil)
		return
	}
	u, err := users.GetFromHTTP(r)
	if err != nil {
		log.Err(err).Msg("unauthorized access")
		out.RenderForbidden("unauthorized access")
		return
	}
	log.Debug().Interface("userdata", u).Msg("user data object")
	// TODO: Lookup user perms by endpoint and allow / deny
	log.Info().Msgf("REQUEST: %s %s", r.RequestURI, r.Method)
	handler.Handle(&out, r, u)
}
