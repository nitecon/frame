package web

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/julienschmidt/httprouter"
	"github.com/rs/zerolog/log"
	"gitlab.com/nitecon/frame/events"
	"gitlab.com/nitecon/frame/rest"
	"gitlab.com/nitecon/frame/users"
)

func getHostName(host string) string {
	if host == "" {
		return "localhost"
	}
	if idx := strings.Index(host, ":"); idx != -1 {
		return host[:idx]
	}
	return host
}

func redirectInsecure(w http.ResponseWriter, r *http.Request) {

	http.Redirect(w, r, "https://"+getHostName(r.Host)+r.RequestURI, http.StatusMovedPermanently)
	return
}

func SetRestRoutes(router *httprouter.Router) *httprouter.Router {
	em := events.SetupEventSystem()
	router.GET("/", GuardRestHandler(RestGetBase, rest.PermSystemRead))
	router.GET("/user/info", GuardRestHandler(users.RestGetUserInfo, rest.PermSystemRead))
	router.GET("/user/authenticate", GuardRestHandler(users.RestAuthenticateUser, rest.PermSystemRead))
	if em != nil {
		router.HandlerFunc(http.MethodPost, "/events", em.PublishHandler)
		router.HandlerFunc(http.MethodGet, "/events", em.SubscriptionHandler)
	}

	return router
}

func Start(httpPort, sslPort int, useSSL bool, sslKey, sslCert string, router *httprouter.Router) {
	log.Info().Msgf("starting web interface...")
	var listenPort = fmt.Sprintf(":%d", httpPort)
	if useSSL {
		go http.ListenAndServe(listenPort, http.HandlerFunc(redirectInsecure))
		var httpsListenPort = fmt.Sprintf(":%d", sslPort)
		log.Fatal().Err(http.ListenAndServeTLS(httpsListenPort, sslCert, sslKey, router)).Msg("failed to initialize secure server")
	}
	log.Fatal().Err(http.ListenAndServe(listenPort, router)).Msg("failed to initialize secure server")
}
