# frame

import with: gitlab.com/nitecon/frame

## Getting started

First load your config (toml) by path, which will also set up zero logger based on debug:

'configFilePath' is a string to the path on disk where the file exists
'yourConfigInterface' should be the pointer to your config interface to unmarshall the config into
'useDebug' is a boolean to indicate whether you want to run debug mode for logs.

### Note this will return an error if the file cannot be read / parsed and &yourconfiginterface will be nil
```
frame.ReadConfigFromFile(configfilePath, &yourConfigInterface, useDebug)

```

## Database setup
Next up is initializing your database for use.
Since this implementation uses sqlite enter the path where the file must exist
```
frame.SetupDB(dbpath string)
```

This will also create a users table, roles table and permissions table.  First user will also be injected for use.
This is to ensure rbac will function properly within the application.

## Add your custom routes
Use the setupRouter function like below to initialize the base routes and add your own:
See web/handler.go on how to create your own guard handlers with permissions

```
router := frame.GetRouter()
```

## Finally start the app

To finalize intialization and start the event system under /events (post|get) make sure to run:

Please note: router should have been retrieved in the frame.GetRouter as shown above... just pass it in.

```
frame.Start(httpPort, sslPort int, useSSL bool, sslKey, sslCert string, router)
```

Start command will check for signals and log shut down and eventually provide a system event.

## To get access to to the event clients do:
(import should be frame.events)
First param is your events location note that you must have the hostname, relative path is /events
Second param is the category you want to subscribe to.
```
c, err := events.GetClient("https://localhost/events", "system")
```

## To publish a topic to the event system:
```
em := events.GetEventManager()
if em != nil{
    em.Publish("system", "shutdown")
}