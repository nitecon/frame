package users

import (
	"fmt"
	"net/http"
	"sync"

	"github.com/gofrs/uuid"
	"github.com/rs/zerolog/log"
	"gitlab.com/nitecon/frame/dbase"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

const (
	bufferSize = 8
)

var (
	userData     map[string]*User
	userDataLock = new(sync.RWMutex)
)

// User is the main struct which contains all user data that is stored for a user session, all of which is encrypted and stored in the user JWT.
type User struct {
	gorm.Model
	Firstname string
	Lastname  string
	Email     string
	Password  string `gorm:"type:varchar(256)"`
	Roles     []Role `gorm:"many2many:users_roles"`
}

func getDefaultPassword() string {
	hash, err := bcrypt.GenerateFromPassword([]byte("admin"), bcrypt.DefaultCost)
	if err != nil {
		log.Fatal().Err(err).Msg("failed to retrieve default password... bcrypt cannot fail...")
		return ""
	}
	return string(hash)
}

func SetupUsers(db *gorm.DB) {
	// Migrate the schema
	db.AutoMigrate(&User{})
	adminUser := User{}
	db.First(adminUser, 1)
	if len(adminUser.Email) == 0 {
		baseRole := &Role{}
		db.First(baseRole)
		db.Create(&User{Firstname: "Super", Lastname: "Admin", Email: "admin@localhost", Password: getDefaultPassword(), Roles: []Role{*baseRole}})
	}
}

func Get(token string) (*User, error) {
	userDataLock.RLock()
	defer userDataLock.RUnlock()
	if userData == nil {
		// we have no users in memory, so all users need to be re-authenticated.
		return nil, fmt.Errorf("authentication required")
	}
	ud := userData[token]
	if ud == nil {
		return nil, fmt.Errorf("invalid token")
	}
	return ud, nil
}

// GetFromHTTP will retrive the user's data via request header User-Token.
func GetFromHTTP(r *http.Request) (*User, error) {
	token := r.Header.Get("User-Token")
	if token == "" {
		return nil, fmt.Errorf("invalid user")
	}
	return Get(token)
}

// CacheUser saves the currently logged-in user into memory.
func CacheUser(token string, user *User) error {
	userDataLock.Lock()
	defer userDataLock.Unlock()
	if user == nil {
		return fmt.Errorf("invalid user")
	}
	userData[token] = user
	return nil
}

func Authenticate(email, password string) (string, error) {
	db := dbase.Get()
	usr := &User{}
	res := db.First(usr, "email = ?", email)
	if res.Error != nil {
		log.Error().Err(res.Error).Msgf("error finding user: %s", email)
		return "", res.Error
	}
	if err := bcrypt.CompareHashAndPassword([]byte(usr.Password), []byte(password)); err != nil {
		log.Error().Err(err).Msgf("authentication failed for: %s", email)
		return "", err
	}
	log.Info().Msgf("authentication successful for: %s", email)

	u2, err := uuid.NewV4()
	if err != nil {
		log.Error().Err(err).Msg("failed to generate UUID")
		return "", err
	}
	err = CacheUser(u2.String(), usr)
	return u2.String(), err
}
