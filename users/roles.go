package users

import (
	"gitlab.com/nitecon/frame/dbase"
	"gorm.io/gorm"
)

// Permissions is the struct used for permissions used by rbac.
type Role struct {
	gorm.Model
	Name        string `gorm:"type:varchar(96)"`
	Description string
	Permissions []Permission `gorm:"many2many:roles_permissions"`
}

func SetupRoles(db *gorm.DB) {
	// Migrate the schema
	db.AutoMigrate(&Role{})
	globRead := &Role{}
	db.First(globRead, 1)
	if len(globRead.Name) == 0 {
		perm1 := []Permission{}
		db.Find(&perm1)
		db.Create(&Role{Description: "Administrator Role", Name: "admin", Permissions: perm1})
	}
}

func GetAllRoles() []Role {
	roleList := []Role{}
	dbase.Get().Find(&roleList)
	return roleList
}
