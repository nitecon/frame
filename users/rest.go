package users

import (
	"encoding/json"
	"net/http"

	"gitlab.com/nitecon/frame/rest"
)

type UData struct {
	Firstname string `json:"firstName"`
	Lastname  string `json:"lastName"`
	Email     string `json:"email"`
	Roles     []Role `json:"roles"`
}

func (s *UData) Scan(sd *User) {
	if sd != nil {
		s.Firstname = sd.Firstname
		s.Lastname = sd.Lastname
		s.Email = sd.Email
		s.Roles = sd.Roles
	}
}

type AuthData struct {
	Email string `json:"email"`
	Pass  string `json:"password"`
}

type AuthResult struct {
	Token  string `json:"token"`
	Result string `json:"result"`
}

// RestGetBase returns the base rest response at /.
func RestGetUserInfo(w *rest.Data, r *http.Request, u *User) {
	//q := rest.NewQuery(r))
	// use query to parse params etc.
	s := &UData{}
	// use local scan function to update response struct (StatusData)
	if u != nil {
		s.Scan(u)
		w.RenderData(s)
		return
	}
	w.RenderBadRequest("no valid user, try authenticating again")
}

// RestGetBase returns the base rest response at /.
func RestAuthenticateUser(w *rest.Data, r *http.Request, u *User) {
	//q := rest.NewQuery(r)
	ad := &AuthData{}
	err := json.NewDecoder(r.Body).Decode(ad)
	if err != nil {
		w.RenderUnauthorized(&AuthResult{Token: "", Result: err.Error()})
		return
	}
	token, err := Authenticate(ad.Email, ad.Pass)
	if err != nil {
		w.RenderUnauthorized(&AuthResult{Token: "", Result: err.Error()})
		return
	}
	w.RenderData(&AuthResult{Token: token, Result: "success"})
}
