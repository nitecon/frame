package users

import (
	"gitlab.com/nitecon/frame/dbase"
	"gorm.io/gorm"
)

// Permissions is the struct used for permissions used by rbac.
type Permission struct {
	gorm.Model
	RoleID      uint
	Description string
	Value       string `gorm:"type:varchar(96)"`
}

func SetupPerms(db *gorm.DB) {
	// Migrate the schema
	db.AutoMigrate(&Permission{})
	globRead := &Permission{}
	db.First(globRead, 1)
	if len(globRead.Value) == 0 {
		db.Create(&Permission{Description: "Global Read Permission", Value: "global.read"})
		db.Create(&Permission{Description: "Global Write Permission", Value: "global.write"})
		db.Create(&Permission{Description: "Global Update Permission", Value: "global.update"})
		db.Create(&Permission{Description: "Global Delete Permission", Value: "global.delete"})
	}
}

func GetAllPerms() []Permission {
	permList := []Permission{}
	dbase.Get().Find(&permList)
	return permList
}
