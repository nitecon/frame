module gitlab.com/nitecon/frame

go 1.19

require (
	github.com/gofrs/uuid v4.0.0+incompatible
	github.com/rs/zerolog v1.28.0
	gorm.io/gorm v1.24.2
)

require (
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/mattn/go-sqlite3 v1.14.16 // indirect
)

require (
	github.com/BurntSushi/toml v1.2.1
	github.com/jcuga/golongpoll v1.3.0
	github.com/julienschmidt/httprouter v1.3.0
	github.com/mattn/go-colorable v0.1.12 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	golang.org/x/crypto v0.3.0
	golang.org/x/sys v0.2.0 // indirect
	golang.org/x/text v0.5.0
	gorm.io/driver/sqlite v1.4.3
)
