package rest

import (
	"encoding/json"
	"net/http"

	"github.com/rs/zerolog/log"
)

const PermSystemRead = "global_read"

// Data provides a central way to handle responses from a rest api
type Data struct {
	data   Content
	Status int
	Writer http.ResponseWriter
}

// Content is the response data that is written to the client.
type Content struct {
	Errors  []string    `json:"errors,omitempty"`
	Result  interface{} `json:"result"`
	Message string      `json:"message"`
}

// PaginatedResponse is to be used for a standardized way of setting large lists of response data.
type PaginatedResponse struct {
	Total      int         `json:"total,omitempty"`
	PageSize   int         `json:"page_size"`
	PageNum    int         `json:"page_number"`
	LastPage   int         `json:"last_page,omitempty"`
	IsLastPage bool        `json:"lastpage"`
	Items      interface{} `json:"items"`
}

// Header is implemented to satisfy http.ResponseWriter
func (rst *Data) Header() http.Header {
	return rst.Writer.Header()
}

// WriteHeader is implemented to satisfy http.ResponseWriter
func (rst *Data) WriteHeader(status int) {
	rst.Writer.WriteHeader(status)
}

// Write is implemented to satisfy http.ResponseWriter
func (rst *Data) Write(data []byte) (int, error) {
	return rst.Writer.Write(data)
}

// GetBodyDataForTesting is a utility function for unit tests to return the data that is sent for writing to the responseWriter.
func (rst *Data) GetBodyDataForTesting() Content {
	return rst.data
}
func (rst *Data) write() {
	d, err := json.Marshal(rst.data)
	if err != nil {
		rst.Status = http.StatusInternalServerError
		rst.Write([]byte("Critical error"))
		log.Error().Err(err)
		return
	}
	
	if rst.Writer.Header().Get("Content-Type") == "" {
		rst.Writer.Header().Set("Content-Type", "application/json")
	}

	rst.WriteHeader(rst.Status)
	rst.Write(d)
	return
}

// RenderData will output the result for the user to consume.
func (rst *Data) RenderData(d interface{}) *Data {
	rst.data.Result = d
	rst.Status = http.StatusOK
	rst.write()
	return rst
}

// RenderDataWithMsg will output the result with responseText for the user to consume.
func (rst *Data) RenderDataWithMsg(d interface{}, msg string) *Data {
	rst.data.Result = d
	rst.Status = http.StatusOK
	rst.data.Message = msg
	rst.write()
	return rst
}

// RenderCreated will set the status code to 201 (for successful resource creates) output the result,responseText for the user to consume
func (rst *Data) RenderCreated(d interface{}, msg string) *Data {
	rst.data.Result = d
	rst.Status = http.StatusCreated
	rst.data.Message = msg
	rst.write()
	return rst
}

// RenderAccepted will set the status code to 202 and output the result,responseText for the user to consume
func (rst *Data) RenderAccepted(d interface{}, msg string) *Data {
	rst.data.Result = d
	rst.Status = http.StatusAccepted
	rst.data.Message = msg
	rst.write()
	return rst
}

// RenderNoContent just sets the status to 204 and returns
func (rst *Data) RenderNoContent() *Data {
	rst.WriteHeader(http.StatusNoContent)
	return rst
}

// RenderBadRequest will produce a standardized error output indicating that the data request could not be marshalled correctly etc.
func (rst *Data) RenderBadRequest(msg string) *Data {
	rst.data.Errors = []string{msg}
	log.Info().Msgf("Bad request: %s", msg)
	rst.data.Message = msg
	rst.Status = http.StatusBadRequest
	rst.write()
	return rst
}

// RenderValidationFailure will produce a standardized error output indicating that the data request failed validation.
func (rst *Data) RenderValidationFailure(errors []string) *Data {
	rst.data.Errors = errors
	log.Info().Msgf("Bad request: %s", "Validation failed.")
	rst.Status = http.StatusBadRequest
	// should this set message to the errors joined instead??
	rst.data.Message = "Validation failed"
	rst.write()
	return rst
}

// RenderUnauthorized renders a response with a 401 unauthorized header
func (rst *Data) RenderUnauthorized(data interface{}) *Data {
	rst.Status = http.StatusUnauthorized
	rst.data.Result = data
	rst.data.Message = "Unauthorized"
	rst.write()
	return rst
}

// RenderServerError would be used when there is an error trying to show data to a customer that otherwise should be working, like corrupted data on our side.
func (rst *Data) RenderServerError(msg string) *Data {
	rst.Status = http.StatusInternalServerError
	rst.data.Message = msg
	rst.write()
	return rst
}

// RenderNotFound is used to notify the user that nothing was found in the database.
func (rst *Data) RenderNotFound(msg string) *Data {
	log.Debug().Msgf("Not found: %s", msg)
	rst.Status = http.StatusNotFound
	rst.data.Message = msg
	rst.write()
	return rst
}

// RenderForbidden is used to notify the user that the operation is Forbidden to the user.
func (rst *Data) RenderForbidden(msg string) *Data {
	log.Debug().Msgf("Forbidden: %s", msg)
	rst.Status = http.StatusForbidden
	rst.data.Message = msg
	rst.write()
	return rst
}

// RenderPaginated provides a method for rendering a list of items with calculated page values, you just need to provide the size of teh page and which page the customer is on.
func (rst *Data) RenderPaginated(d interface{}, pageSize, pageNum, totalItems int, isLastPage bool) *Data {
	rst.data.Result = BuildPaginatedResponse(d, pageSize, pageNum, totalItems, isLastPage)
	rst.Status = http.StatusOK
	rst.write()
	return rst
}

// RenderAppError is used to notify the user with all the errors occurred in the system.
func (rst *Data) RenderAppError(appErrors []string, message string, code int) *Data {
	rst.data.Errors = appErrors
	rst.data.Message = message
	rst.Status = code
	rst.write()
	return rst
}

// BuildPaginatedResponse builds a list of items with calculated page values, you just need to provide the size of the page and which page the customer is on.
func BuildPaginatedResponse(d interface{}, pageSize, pageNum, totalItems int, isLastPage bool) PaginatedResponse {
	items := PaginatedResponse{Items: d, PageNum: pageNum, PageSize: pageSize, IsLastPage: isLastPage}
	if totalItems > 0 {
		items.Total = totalItems
		if pageSize == 0 {
			pageSize = 1
		}
		items.LastPage = totalItems / pageSize
		if totalItems%pageSize > 0 {
			items.LastPage++
		}
	}
	if items.PageNum > items.LastPage && items.LastPage > 0 {
		items.PageNum = items.LastPage
	}
	// Must do && here to make sure if the new bool is set that it doesn't get overwritten by the 0's that may be unset for total / pagenum.
	if items.PageNum == items.LastPage && totalItems > 0 {
		items.IsLastPage = true
	}

	return items
}
