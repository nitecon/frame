package rest

import (
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"golang.org/x/text/transform"
	"golang.org/x/text/unicode/norm"
)

// StandardQuery provides a wrapper around url.Values and provides several methods for getting common query params
type StandardQuery struct {
	url.Values
}

func NewQuery(req *http.Request) *StandardQuery {
	return &StandardQuery{Values: req.URL.Query()}
}

// ParseUint parses a uint from value at the given query key
func (sq *StandardQuery) ParseUint(queryKey string) (uint64, error) {
	value, err := strconv.ParseUint(sq.Values.Get(queryKey), 10, 0)
	if err != nil {
		// if there was an error parsing the value then just return the default value
		return 0, err
	}
	return value, nil
}

// ParseUintWithDefault parses a uint from the value at the given query key and if an error occurs it returns the provided defaultValue
func (sq *StandardQuery) ParseUintWithDefault(queryKey string, defaultValue uint64) uint64 {
	value, err := sq.ParseUint(queryKey)
	if err != nil {
		// if there was an error parsing the value then just return the default value
		return defaultValue
	}
	return value
}

// ParseUintWithOverride parses a uint from the value at the given query key allows specifying a maximum value as well
// an override for allowing values higher than the maximum
// defaultValue with be used as the value if the value at the given query key is not a valid uint
// max will limit results to the specified maximum value unless override is non-zero
// override will override max up to the override value (only if nonzero)
func (sq *StandardQuery) ParseUintWithOverride(queryKey string, defaultValue uint64, max uint64, overrideMax uint64) uint64 {
	actualDefaultValue := defaultValue

	// if an override was provided then it also overrides the default value
	if overrideMax > 0 {
		actualDefaultValue = overrideMax
	}

	value := sq.ParseUintWithDefault(queryKey, actualDefaultValue)

	// don't override the given maximum value
	if overrideMax == 0 {
		if value > max {
			return max
		}
		return value
	}

	// allow overriding the given maximum only up to the overridden maximum
	if value > overrideMax {
		return overrideMax
	}

	// otherwise return the value that was parsed directly
	return value
}

// ParseUint32 parses a uint32 from value at the given query key
func (sq *StandardQuery) ParseUint32(queryKey string) (uint32, error) {
	value64, err := sq.ParseUint(queryKey)
	if err != nil {
		// if there was an error parsing the value then just return the default value
		return 0, err
	}
	return uint32(value64), nil
}

// Advanced Unicode normalization and filtering,
// see http://blog.golang.org/normalization and
// http://godoc.org/golang.org/x/text/unicode/norm for more
// details.
func stripCtlAndExtFromUnicode(str string) string {
	isOk := func(r rune) bool {
		return r < 32 || r >= 127
	}
	// The isOk filter is such that there is no need to chain to norm.NFC
	t := transform.Chain(norm.NFKD, transform.RemoveFunc(isOk))
	// This Transformer could also trivially be applied as an io.Reader
	// or io.Writer filter to automatically do such filtering when reading
	// or writing data anywhere.
	str, _, _ = transform.String(t, str)
	return str
}

func (sq *StandardQuery) GetStringParam(key string) string {
	reqVal := sq.GetQueryParamValue(key)
	return stripCtlAndExtFromUnicode(reqVal)
}

// GetQueryParamValue gets the first non-empty query param value using the given list of keys. It traverses keys
// in-order stops as soon as a non-empty value is found for the corresponding key. If no non-empty key is found,
// it returns an empty string
func (sq *StandardQuery) GetQueryParamValue(keys ...string) string {
	q := sq.Values
	for _, key := range keys {
		value := strings.TrimSpace(q.Get(key))
		if value != "" {
			return value
		}
	}
	return ""
}
