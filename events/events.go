package events

import (
	"net/url"
	"sync"

	"github.com/jcuga/golongpoll"
	"github.com/jcuga/golongpoll/client"
	"github.com/rs/zerolog/log"
)

var (
	manager     *golongpoll.LongpollManager
	managerLock = new(sync.RWMutex)
)

// Set sets the global configuration
func SetupEventSystem() *golongpoll.LongpollManager {
	managerLock.Lock()
	defer managerLock.Unlock()
	mgr, err := golongpoll.StartLongpoll(golongpoll.Options{
		// How many chats per topic to hang on to:
		MaxEventBufferSize: 1000,
		LoggingEnabled:     true,
	})
	if err != nil {
		log.Error().Err(err).Msg("cannot initialize event system")
	}
	manager = mgr
	return manager
}

func GetEventManager() *golongpoll.LongpollManager {
	managerLock.RLock()
	defer managerLock.RUnlock()
	return manager
}

// GetClient will parse the loc (http://127.0.0.1:8101/chatbot/events) with the respective category and return a client for that.
func GetClient(loc, category string) (*client.Client, error) {
	u, err := url.Parse(loc)
	if err != nil {
		return nil, err
	}

	return client.NewClient(client.ClientOptions{
		SubscribeUrl:   *u,
		Category:       category,
		LoggingEnabled: true,
	})
}
