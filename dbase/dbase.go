package dbase

import (
	"sync"

	"gitlab.com/nitecon/frame/gormlog"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

var (
	dbv     *gorm.DB
	dbvLock = new(sync.RWMutex)
)

// Set sets the global configuration
func Set(dbc *gorm.DB) *gorm.DB {
	dbvLock.Lock()
	defer dbvLock.Unlock()
	dbv = dbc
	return dbv
}

// Get function returns the currently set active db to be used.
func Get() *gorm.DB {
	dbvLock.RLock()
	defer dbvLock.RUnlock()
	return dbv
}

func getSaneDBPath(in string) string {
	if in == "" {
		return "app.db"
	}
	return in
}

func InitDatabase(dbPath string) (*gorm.DB, error) {
	zl := &gormlog.Logger{}
	db, err := gorm.Open(sqlite.Open(getSaneDBPath(dbPath)), &gorm.Config{Logger: zl})
	if err != nil {
		panic("failed to connect database")
	}

	return Set(db), nil
}
