package frame

import (
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/BurntSushi/toml"
	"github.com/julienschmidt/httprouter"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/nitecon/frame/dbase"
	"gitlab.com/nitecon/frame/users"
	"gitlab.com/nitecon/frame/web"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

// remember to use config/ReadConfigFromFile directly
func setLogger(useDebug bool) {
	zerolog.TimeFieldFormat = time.RFC3339Nano
	//log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stdout})
	if useDebug {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
		return
	}
	zerolog.SetGlobalLevel(zerolog.InfoLevel)
}

// ReadConfigFromFile is the primary loading function to pull data from the toml config file and sync it.
func ReadConfigFromFile(configfile string, cInterface interface{}, useDebug bool) error {
	setLogger(useDebug)
	configFile, err := os.ReadFile(configfile)
	if err != nil {
		return err
	}
	err = toml.Unmarshal(configFile, cInterface)
	if err != nil {
		return err
	}
	return nil
}

func SetupDB(dbpath string) (*gorm.DB, error) {
	db, err := dbase.InitDatabase(dbpath)
	if err != nil {
		log.Error().Msgf("error with database: (%s)", dbpath)
		return nil, err
	}
	db.Logger.LogMode(logger.Error)
	log.Info().Msg("validating permissions data table...")
	users.SetupPerms(db)
	log.Info().Msg("validating roles data table...")
	users.SetupRoles(db)
	log.Info().Msg("validating users data table...")
	users.SetupUsers(db)

	return db, nil
}

func handleSignals(signal os.Signal) {
	if signal == syscall.SIGTERM {
		log.Info().Msg("Got kill signal, closing down...")
		// Add flushing here...
		log.Info().Msg("Program will terminate now.")
		os.Exit(0)
	} else if signal == syscall.SIGINT {
		log.Info().Msg("Got interrupt signal, closing down...")
		// Add flushing here...
		log.Info().Msg("Program will terminate now.")
		os.Exit(0)
	} else {
		log.Debug().Msgf("Ignoring signal: ", signal)
	}
}

func GetRouter() *httprouter.Router {
	router := httprouter.New()
	router.GlobalOPTIONS = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Header.Get("Access-Control-Request-Method") != "" {
			// Set CORS headers
			header := w.Header()
			header.Set("Access-Control-Allow-Methods", header.Get("Allow"))
			header.Set("Access-Control-Allow-Origin", "*")
		}

		// Adjust status code to 204
		w.WriteHeader(http.StatusNoContent)
	})
	router = web.SetRestRoutes(router)
	return router
}

func Start(httpPort, sslPort int, useSSL bool, sslKey, sslCert string, router *httprouter.Router) {
	go web.Start(httpPort, sslPort, useSSL, sslKey, sslCert, router)
	sigchnl := make(chan os.Signal, 1)
	signal.Notify(sigchnl)
	exitchnl := make(chan int)

	go func() {
		for {
			s := <-sigchnl
			handleSignals(s)
		}
	}()

	exitcode := <-exitchnl
	log.Info().Msg("shutting down in 5s...")
	time.Sleep(time.Second * 5)
	os.Exit(exitcode)
}
